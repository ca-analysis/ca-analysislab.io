import httpclient
import json
import sequtils

let headers = newHttpHeaders({
    "content-type": "application/json",
    "authorization": "Basic bUFJRnB1b1RsaGkxYXZXS2lCN0x1VE9ucTN5ZkZMazdYRFl2M2RVYTo="
})

let client = newHttpClient()
client.headers = headers

let baseurl = "https://api.companieshouse.gov.uk"


type
    CompaniesData = object
        appointments: seq[Appointment]
        companies: seq[Company]
        officers: seq[Officer]

    Officer = object
        officer_name: string
        appointments_link: string #this is basically the officer ID

    Appointment = object
        appointments_link: string
        company_number: string      

    Company = object
        company_number: string
        company_name: string
        filing_history: JsonNode


# awkward to use method
proc request_from_path_with_client(client: HttpClient, baseurl: string, path: string, page: int): JsonNode = 
    client.get(baseurl & path & "?items_per_page=50&p=" & $page).body.parseJson()

# curry in to nice to use method
proc create_get_request(client: HttpClient, baseurl: string, page: int = 1): proc(path:string): JsonNode = 
    (proc(path:string):JsonNode = request_from_path_with_client(client, baseurl, path, page))

# naming could do with some work but "get_json_from_companies_house" seemed a bit OTT.
let get_data = create_get_request(client, baseurl)

let get_paged_data = (client, baseurl): JsonNode =
    let first_page = get_data(client, baseurl)
    first_page["pages"]

proc crawl(): auto =

    var officers: seq[Officer] = @[]
    var appointments: seq[Appointment] = @[]
    var companies: seq[Company] = @[]


    #echo "getting data..."
    let company_number = "05514098"
    let officers_link_path = get_data("/company/" & company_number)["links"]["officers"].getStr()
    let officers_list = get_data(officers_link_path)["items"]

    for officer_item in officers_list:
        #echo officer_item.pretty()
        
        let officer = Officer(appointments_link: officer_item["links"]["officer"]["appointments"].getStr(),
                              officer_name: officer_item["name"].getStr())
        officers.add(officer)

        let officer_appointments_link_path = officer_item["links"]["officer"]["appointments"].getStr()
        #echo "=== ", officer_item["name"], " ===\n"
        let officer_appointments = get_data(officer_appointments_link_path)["items"]
        for officer_appointment in officer_appointments:
            #echo officer_appointment["appointed_to"].pretty()
            #echo ""

            let company_data = get_data(officer_appointment["links"]["company"].getStr())
            let filing_history = get_data(company_data["links"]["filing_history"].getStr())
            companies.add(Company(company_name: officer_appointment["appointed_to"]["company_name"].getStr(),
                                  company_number: officer_appointment["appointed_to"]["company_number"].getStr(),
                                  filing_history: filing_history))
        
            appointments.add(Appointment(company_number: officer_appointment["appointed_to"]["company_number"].getStr(), appointments_link: officer.appointments_link))

    officers = officers.deduplicate()
    appointments = appointments.deduplicate()
    companies = companies.deduplicate()

    let companies_data = CompaniesData(officers:officers, companies:companies, appointments: appointments)
    echo (%* companies_data).pretty()


crawl()

#TODO: implement max degrees
# max_degrees_of_separation = 1

# officers_link_path = requests.get(baseurl + "/company/10911848", headers=headers).json()['links']['officers']
# officers_list = requests.get(baseurl + officers_link_path, headers=headers).json()['items']

# for officer_item in officers_list:
#     officer_appointments_link_path = officer_item['links']['officer']['appointments']
#     officer_appointments = requests.get(baseurl + officer_appointments_link_path, headers=headers).json()
#     pprint(officer_appointments)
#     for appointment in officer_appointments:
        