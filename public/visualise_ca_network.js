$(window).ready(function(){
    console.log("Starting up...");
    generate_ca_graph();
    generate_ca_data_table();
});

function generate_ca_graph() {


    var svg = d3.select("#ca_graph");
    var width = +svg.attr("width");
    var height = +svg.attr("height");
    console.log("what this thinks is SVG: " + svg)
    var color = d3.scaleOrdinal(d3.schemeCategory10);

    var force = d3.forceManyBody();
    force.strength(-7);
    force.distanceMin(1);
    force.distanceMax(250);

    var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().id(function(d) { return d.id; }))
    .force("charge", force)
    .force("center", d3.forceCenter(width / 2, height / 2));

    var company_nodes = companies_data.companies
        .map(function(c) { return {id: c.company_name, group: 1}});

    var officer_nodes = companies_data.officers
        .map(function(o) {return {id: o.officer_name, group: 3}});

    var nodes = company_nodes.concat(officer_nodes);
    var links = companies_data.appointments
        .map(function(a) { return {
            source: companies_data.companies.find(function(c) { return c.company_number == a.company_number }).company_name,
            target: companies_data.officers.find(function(o) { return o.appointments_link == a.appointments_link }).officer_name,
            value: 5,
         }});

    console.log(officer_nodes[0], company_nodes[0], links[0]);
    
    var graph = {
        "nodes": nodes, "links": links
    };
    var link = svg.append("g")
    .attr("class", "links")
    .selectAll("line")
    .data(graph.links)
    .enter().append("line")
    .attr("stroke-width", function(d) { return Math.sqrt(d.value); });

    var node = svg.append("g")
    .attr("class", "nodes")
    .selectAll("circle")
    .data(graph.nodes)
    .enter().append("circle")
    .attr("r", 5)
    .attr("fill", function(d) { return color(d.group); })
    .call(d3.drag()
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended));

    node.append("title")
    .text(function(d) { return d.id; });

    function ticked() {

        link
            .attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });
    
        node
            .attr("cx", function(d) { return d.x; })
            .attr("cy", function(d) { return d.y; });
    }

    simulation
        .nodes(graph.nodes)
        .on("tick", ticked);

    simulation.force("link")
        .links(graph.links);

    function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
    }
    
    function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }
    
    function dragended(d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }
}





function generate_ca_data_table() {
    $("#ca_data_table").html("<span>test</span>");
    var table = $("<table class='table table-hover table-condensed'/>");
    var tbody = $("<tbody class=''/>");
    table.append(tbody);

    var header = $("<tr/>")
        .append($("<th/>").text("Company name"))
        .append($("<th/>").text("Directors"))
        .append($("<th/>").text("Filing history"));

    tbody.append(header);

    var companies = companies_data.companies;
    var officers = companies_data.officers;
    var appointments = companies_data.appointments; 

    for(var companyIdx in companies) {
        console.log("Adding company: " + companies[companyIdx].company_name);
        var company_number = companies[companyIdx].company_number
        var appointments_links = appointments
            .filter(function(appt) { return appt.company_number === company_number })
            .map(function(appt) { return appt.appointments_link });

        var company_officers = officers.filter(function(o) { return appointments_links.findIndex(
            function(al) {return o.appointments_link === al}) >= 0});

        var company_officer_names = company_officers.map(function(o) { return o.officer_name}).join(", ")
        
        
        

        console.log(company_officers);

        var row = $("<tr/>")
            .append($("<td/>").text(companies[companyIdx].company_name))
            .append($("<td/>").text(company_officer_names))
            .append($("<td/>").text(companies[companyIdx].filing_history.total_count + " entries: view"))
        tbody.append(row);
    }

    $("#ca_data_table").append(table);
}